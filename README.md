# mams-hello

This is a simple MAMS project that demonstates agent-agent interaction via REST.

In the code, an instance of `Main` is created, called `main`.  This agent sets up the MAMS infrastructure, but does not
register itself as a MAMS agent (so it cannot create a Hypermedia Body). Instead, it creates an agent called `hello` that is
an instance of the `Hello` agent class.

The `hello` agent registers itself as a MAMS agent, creates a body (called a base) and creates a simple item resource that is
based on the `Test` java class (see `src/main/java`).

The `main` agent sleeps for 1/2 a second and then performs a HTTP Get Request on the URL associated with the simple item resource
created by the `hello` agent.  Because the `hello` agent is a Passive MAMS agent, it does not know about the request, the MAMS
infrastructure simply returns the current state of the resource which is then displayed by the `main` agent.

Next, the main agent sends a HTTP Put Request to replace the current contents of hello's simple item resource with some new
content, and then sends anothe HTTP Get Request to confirm the update.

## Running the Code
To run this program, download the project from Git, go to the root directory of the project and type:

```mvn compile astra:deploy```

## Dockerizing the Code
To create a docker image based on this code. run:

```mvn package```

Followed by:

```docker build -t mams-hello:latest .```

To run the docker image, type:

```docker run mams-hello:latest```

To run the code using docker-compose, type:

```docker-compose up --build```
