FROM openjdk:11
ENV SERVICE_HOST localhost
ENV SERVICE_PORT 9000
COPY target/*jar-with-dependencies.jar /mams-hello.jar
CMD java -jar /mams-hello.jar